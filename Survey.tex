%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 11 pt, conference]{ieeeconf}  % Comment this line out
                                                          % if you need a4paper
%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4
                                                          % paper

\IEEEoverridecommandlockouts                              % This command is only
                                                          % needed if you want to
                                                          % use the \thanks command
\overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage[utf8]{inputenc}
 
\usepackage{listings}
\usepackage{xcolor}
 
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\def\code#1{\texttt{#1}}

% The following packages can be found on http:\\www.ctan.org
%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
The Cost of High Performance
}

\author{ Shant Hairapetian }




\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

Parallelism is the future of high-performance computing and Nvidia's 
CUDA is the leading technology in this space. The CUDA API is very low level
and forces developers to do much of the heavy lifting when it comes to optimizing parallelization. This level of fine-grained control also makes it more difficult to write correct code. With general purpose GPU (GPGPU) programming being applied to a steadily growing array of domains, we must be wary of the security implications of potentially insecure code running on a platform whose integrity is already in question. Writing our GPU code in a higher level language which conveys the parallel semantics lends itself to writing less buggy and thus more secure code.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}


The general consensus seems to be that Moore’s law is coming to an end \cite{c1};  try as we might, we just can’t optimize away the laws of physics. The end result is that CPU clock speed is about as fast as it’s ever going to get, while the demand for better and better performance is ever increasing. Barring a paradigm shift in computing, the only solution we have to this problem is parallel/multi-core computation; we may not be able to make faster cores, but we can make more. 

In the late 1990’s, the NVIDIA corporation unveiled their first line of GeForce GPU’s (Graphics Processing Units) which unlike the dual core CPU’s of the time, leveraged 8 cores to greatly accelerate parallel computation. At the time, 3D video rendering was the chief application application for these devices and as such, developers seeking to utilize the GPU were forced to use 3D graphics libraries such as OpenGL and Direct3D which had notoriously steep learning curves \cite{c2} . As these libraries were highly specialized, they made GPGPU (General Purpose GPU) programming that much more difficult. In response, NVIDIA released CUDA API in 2007 which extended the C, C++ and Fortran languages to support the GPU instruction set. By supplying users a simpler (relatively speaking) API, CUDA for the first time empowered regular developers to write massively parallel code in a language they were familiar with. 

This lower barrier to entry combined with the steadily growing performance of the chips, created the necessary conditions for the crypto-mining boom which was soon to follow. Bitcoin, launched in 2009, allowed users to utilize their own machine to “mine” currency by solving increasingly difficult cryptographic problems. Blockchain, the underlying technology behind Bitcoin and other crypto-currencies is an inherently slow process however, it is parallelizable.  When users figured out that GPU’s allowed them mine coins at a far faster rate than CPU’s, it quickly became nigh impossible to buy a GPU anywhere.

Another factor in the rise of GPU computing has been the increasing ubiquity of machine learning and specifically a subset of the field called deep learning \cite{c3} (DL). Though the idea of DL can be traced as far back as 1943 it was extremely inefficient in practice when compared to other learning methods. That is until the advent of the modern GPU. Engineers discovered that the many layers of training involved in DL could be parallelized \cite{c3} and offloaded to the GPU, thus drastically reducing training time and enabling real world DL applications.

As GPGPU programming has entered the zeitgeist of the software industry, heterogeneous software applications (software running on both CPU and GPU) have made their way not only into the cloud, but into many sensitive and mission-critical domains such as health \cite{c4}, finance \cite{c5}, space and defense.


\section{Platform (In)Security}
\subsection{Architecture}
The CUDA architecture can be summed up as follows:
\begin{itemize}


\item A single binary containing both host (CPU) and device (GPU) code.
\item The CUDA user library (closed source)
\item GPU Driver (closed source)
\item GPU Hardware

\end{itemize}

The NVCC compiler builds both host and device code and links them
into a single binary. In order for the GPU to see any data, the user
must use the CUDA API to copy data from main memory to the GPU device memory. Then the host code invokes a kernel which is a function that is run in parallel on the GPU. The kernel is then run on a collection of threads called a thread block. Threads within the same thread block, and ONLY threads in the same block share the same local memory and can communicate seamlessly. 

Once the solution has been computed, the resulting data is then copied back to host memory. The implication of this architecture is that GPU computations suffer much greater latency than their CPU counterparts as any data used must first be copied in then out of device memory.

The reason this is troubling from a security perspective is that once data is copied to the device, the operating system can no longer guarantee its privacy or integrity \cite{c6}. On the CPU, the OS utilizes well documented, open source (in the case of Linux, currently the most widely deployed GPU platform) and battle tested methods of memory isolation and access control. All of that responsibility must then be shouldered by the NVIDIA GPU driver.

Unfortunately however, NVIDIA (as many others in industry)  have prioritized the maintaining of trade secrets over the security of their products. In keeping their drivers closed source and their security documentation sparse, NVIDIA hopes to accomplish security by obscurity. The merits of this approach can be observed in the list of breaches and vulnerabilities CUDA has accrued over the years.

To be fair to NVIDIA, at the advent of the technology the main application of GPU processing was 3D rendering. This meant that any data or code transferred to the GPU would be coming directly from the user (i.e. by donwloading a game or installing video rendering software). These assumptions were quickly proven false as GPU's begin being used to offload encryption \cite{c7}, offer shared processing on the cloud and even to render web content. Though obviously useful, these features have created avenues for malicious input to be executed on GPU's. 

The implication for CUDA developers is that it becomes paramount to ensure the correctness of their GPU code to guard against malicious code injection. Remote code execution becomes that much more dangerous given the dubious security record of the underlying architecture.

\subsection{CUDA runtime API}
CUDA developers have two options when writing device code: the driver API and the CUDA runtime API \cite{c8}. The former gives users fine grained control of memory management, and module loading, while the latter provides a (slightly) simpler API which does abstract away SOME of the plumbing such as the configuration and initialization of kernels. The vast majority of users rely solely on the runtime API and as such that is what we will focus on.

As outlined in the previous section, the CUDA architecture necessitates copying data from the CPU to the GPU and back in order for any co-processing to occur. 
\begin{lstlisting}[language=C]
  float *x, *d_x;
  x = (float*)malloc(size);
  
  cudaMalloc(&d_x, size); 
  cudaMemcpy(d_x, x, size, cudaMemcpy...
  plusOne<<<(N+255)/256, 256>>>(N, d_x);
  
  cudaMemcpy(y, d_y, size, cudaMemcpy...
  
\end{lstlisting}

Above is an example of a heterogeneous program expressed in the CUDA C runtime library. \code{cudaMalloc} allocates device memory, \code{sizecudaMemcpy} copies memory between the host and the device, and \code{sizeplusOne} is the kernel which gets executed in parallel on the array \code{d\_x} on the GPU. The parameters in the bracketed syntax denotes to the GPU i) the number of thread blocks to use for the computation and ii) the number of threads in those blocks. This code will result in the array being broken up into a grid of blocks amongst which the elements of the array are distributed. Those thread blocks are then scheduled and run in parallel

\subsection{Latency Hiding}

The extra copying steps combined with the low bandwidth and high latency of the PCI-E bus, means that programmers must be sure that they structure reads and writes \cite{c9} to hide the latency of transfer between host and device. In addition to structuring the reads, it is also important to make sure that they are not redundant (i.e. the same data being copied between devices twice).

To this end, developers must structure their reads and writes to optimize efficiency. This can be done by batching together data transfers between host and device as well as overlapping \cite{c10} data transfer with kernel execution. This is accomplished using the \code{cudaMemcpyAsync} to copy data asynchronously over the bus and only executes the kernel on blocks of the input which have already been copied to the GPU.

Another consideration when it comes to memory management is to align device memory access and ensure that no conflicts exist within a block. Memory access from global memory is aligned to 32, 64 or 128 bit blocks. This means combining reads from thread blocks (the units of execution at runtime are actually slightly finer grain groupings of threads called warps, but I will continue to use thread blocks as part of the example) such that they are aligned to one of those sizes. Miss-alignment of I/O operations can lead to dramatic slowdown \cite{c11} of the program. 

In addition to alignment issues, memory conflicts and race condition are also possible. Alongside global device memory, thread blocks have their own shared memory (closer to the cores) which has much lower latency and is accessible by all of the threads in a thread block. This shared memory is divided evenly into a set of banks. If a single thread is both reading and writing to a bank or more than one thread is accessing a bank at a time, the accesses must be serialized (completed atomically, one after the other) to avoid conflicts. This serialization obviously results in a slowdown \cite{c12} so developers must be mindful to avoid bank conflicts. This is done by orienting reads and writes such that each thread reads the position exactly one space away from position read by the previous thread. As a result memory accesses become sequential as subsequent threads are executed. This is known as coalescing memory.

\section{Controlling Complexity}
\subsection{High Level Languages}
It is apparent that CUDA developers have a lot of complexity to juggle that is orthogonal to the actual algorithm they are working with; They must optimize their code in several different ways simultaneously, while preserving the semantics of the code (i.e. making sure they still get the right answer). This level of complexity makes it extremely difficult and expensive to produce correct, bug-free code. This problem is compounded by the fact that many users of CUDA are scientists who are not primarily software engineers. As a result, we can assume that the low lever, fine grained control provided by the CUDA API is conducive to insecure code that may be vulnerable to arbitrary code execution. 

Since the advent of programming languages, researchers have strived to create languages that express software at a level of abstraction that is readable by humans. Though considering languages such as C and FORTRAN to be "human readable" today, at the time of their release, many people were programming in assembly with punch-cards. After this first generation of high level languages, languages emerged with new features such as object orientation and polymorphism which gave developers a new level of abstraction (closer to human speech) for expressing their programs.

\subsection{Functional Programming}
One class of these emergent modern languages are pure, functional languages. In contrast to other languages such as Java or C++, the basis of pure functional languages is that they express programs as a series of equations. The mathematical structure of these languages makes it easier to reason about and logically prove characteristics of code in written in them. The "pure" in pure functional programming refers to the purity of functions used in the language. This means that pure functional code has a characteristic known referential transparency: any expression can be replaced by it's value and still maintain its meaning. An example of this would the ability to inline the definition of a function for every invocation of it in the code. 

Referential transparency means that global state does not exist and any data a function needs access to must be passed in as a parameter\cite{c13}. This also that given the same input, a function must always return the same output. As pure functional programs are just expressions, they can be executed in parallel or out of order. This flips the conventional parallelism paradigm on its head \cite{c14}: instead of code being serial by default in traditional languages, they are parallelizable by default in pure functional languages. Though this might seem like a silver bullet, pure functional programs which arbitrarily parallelize execution often experience slowdown as a result of overhead. So in contrast to parallelizing imperative programs which involved finding code fragments to parallelize, in pure functional languages, you find the code which MUST be serialized. Though purity does lend itself to parallelization, it present one problem: communication with the outside world. If all you have at your disposal are pure functions it becomes seemingly impossible to interface with a database or a network given that there can be no guarantees that values retrieved from these resources will always have the same output. Languages such as Scala and OCaml allow users to bypass the purity of functions however, languages such as Haskell and Agda, express effectful computations through constructs called monads which emerge from mathematical category theory \cite{c15}.

\subsection{Algebraic Data Types}
Though not exclusive to pure functional languages algebraic data types are often seen in conjunction with them. ADT's allow users to define types by combining smaller primitive types. The types are algebraic because they can be combined into larger types either with product (*) or sum (+). A product type would be the combination of one of more types while a sum type is one of a choice of types. Types can also be recursive and parameterized by generic types. A simple example an algebraic, recursive, higher order (generic) datatype is list.

\begin{lstlisting}[language=Haskell]
  data List a = Nil | Cons a (List a)
\end{lstlisting}

The code above is \code{List} expressed as a Haskell sum type. The constructors on the right hand side of the data definition are the choices of the sum type delimited by the "|" symbol. This is expressing that a list is either Nil (empty), or the combination (or product) of a head and a tail of generic type "a". So we can read this as:
\begin{lstlisting}
  List a = Nil + Cons a * List a
\end{lstlisting}
It may not be clear from this simple example but ADT's allow for complex constraints such as performance characteristics, ownership, and proofs correctness to be expressed in the structure of a type. These constraints are then validated statically on compilation of the program during type-checking. System languages such as Rust \cite{c16} employ the type system to enforce static compile time guarantees of memory safety, making crashes (corruption of the call stack, not a domain error) impossible. A special class of algebraic data types called dependent types are used in languages such as Coq and Agda to encode program correctness in the types, thereby making it possible to have compositional proofs of correctness which can be rearranged as software grows.


\section{Accelerate}
\subsection{API}
Accelerate \cite{c17} is an embedded domain-specific language (EDSL) and runtime for the pure functional programming language Haskell. It provides a series of array operations which map to the CUDA architecture. The Accelerate runtime then compiles GPU specific Haskell code while simultaneously beginning to transfer the input data from host to device in preparation for execute the kernel. This is done to hide transfer latency.

The array functions provided by the library range over the type \code{Array sh e} where \code{sh} is a generic type representing the shape of the data and \code{e} is the type of the individual elements of the array. In addition, Accelerate expresses the transfer of data from the host to the device using the \code{Acc} type constructor:
\begin{lstlisting}[language=Haskell]
  use :: Array sh e -> Acc (Array sh e)
\end{lstlisting}
The type signature of the \code{use} function denotes that we are encasing the array in the \code{Acc} type, which expresses the transfer of data to the GPU. This means that we can embed the location of the data in the type itself and thus type differentiate variables which are on the CPU vs those on the GPU. Once transferred to the device, because of the immutable nature of Haskell data structures, the arrays can be shared amongst any number of threads without a possibility for race conditions.

\subsection{Perfromance Evaluation}
Traditionally, aggressively performance tuning has necessitated fine grain control over memory. Functional languages in particular have been stigmatized as slow. With the advancement of compiler technology however, code optimizations made during compile time have closed the performance in many domains. Chakravarty et al. (2010) benchmarked functions from NVIDIA CUBLAS library (namely, dot-product, Black-Scholes option pricing, and Sparse-matrix vector multiplication) against their Accelerate implementations.

\subsubsection{dot-product}
Because of the fact that Accelerate needs two kernels to implement dot-product functionally whereas CUDA C can use a single one, the native implementation is almost exactly twice as fast.

\subsubsection{Black-Scholes option pricing}
The Black-Scholes option pricing algorithm is used for the modelling for option prices is specific conditions. The Accelerate version did perform slower than the library function but only by ~0.7 ms in the worst case.

\subsubsection{Sparse-matrix vector multiplication}
They then measure their Accelerate implementation of SMV multiplications against several hand tuned CUSP algorithms. Though the accelerate implementation did outperform implementations which did not explorer memory coalescing, it did perform 3x worse than the implementation which used a single thread block per matrix row.



\section{Conclusion}
As we move toward a world of heterogeneous computing where GPU's are doing everything from web encryption to driving cars, it is our responsibility as engineers to ensure the robustness of the technologies we are using at every stage of the pipeline. Though there still might be some performance benefits to handwriting CUDA C, they are overshadowed by the security concerns associated with it.

\addtolength{\textheight}{-12cm}   % This command serves to balance the column lengths
                                  % on the last page of the document manually. It shortens
                                  % the textheight of the last page by a suitable amount.
                                  % This command does not take effect until the next page
                                  % so it should come on the page before the last. Make
                                  % sure that you do not shorten the textheight too much.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{99}

\bibitem{c1} T. N. Theis and H. -. P. Wong, "The End of Moore's Law: A New Beginning for Information Technology," in Computing in Science \& Engineering, vol. 19, no. 2, pp. 41-50, Mar.-Apr. 2017.
\bibitem{c2} E. Angel and D. Shreiner, "Teaching a Shader-Based Introduction to Computer Graphics" in IEEE Computer Graphics and Applications, vol. 31, no. 02, pp. 9-13, 2011.
\bibitem{c3} Kadupitige, K. (2017). "Intersection of HPC and Machine Learning."
\bibitem{c4} NVIDIA, Hewlett Packard Enterprise. 2018. "Modernizing healthcare workloads with virtual GPU technology"
\bibitem{c5} Scott Grauer-Gray, William Killian, Robert Searles, and John Cavazos. 2013. Accelerating financial applications on the GPU. In Proceedings of the 6th Workshop on General Purpose Processor Using Graphics Processing Units (GPGPU-6), John Cavazos, Xiang Gong, and David Kaeli (Eds.). ACM, New York, NY, USA, 127-136.
\bibitem{c6} Roberto Di Pietro, Flavio Lombardi, and Antonio Villani. 2016. CUDA Leaks: A Detailed Hack for CUDA and a (Partial) Fix. ACM Trans. Embed. Comput. Syst. 15, 1, Article 15 (January 2016), 25 pages
\bibitem{c7} Luken, B.P., Ouyang, M., \& Desoky, A.H. (2009). AES and DES Encryption with GPU. ISCA PDCCS.
\bibitem{c8}“Difference between the Driver and Runtime APIs.” NVIDIA Developer Documentation, docs.nvidia.com/cuda/cuda-runtime-api/driver-vs-runtime-api.html.
\bibitem{c9} Harris, Mark, and Mark. “How to Optimize Data Transfers in CUDA C/C++.” NVIDIA Developer Blog, 2 May 2018, devblogs.nvidia.com/how-optimize-data-transfers-cuda-cc/.
\bibitem{c10} Harris, Mark, and Mark. “How to Overlap Data Transfers in CUDA C/C++.” NVIDIA Developer Blog, 24 Apr. 2018, devblogs.nvidia.com/how-overlap-data-transfers-cuda-cc/.
\bibitem{c11} Kim, Dae-Hwan. “Evaluation Of The Performance Of GPU Global Memory Coalescing.” (2017).
\bibitem{c12} Khan, Ayaz \& Al-Mouhamed, Mayez \& Fatayar, A. \& Almousa, Anas \& Baqais, Abdulrahman \& Assayony, Mohammed. (2014). Padding Free Bank Conflict Resolution for CUDA-Based Matrix Transpose Algorithm. International Journal of Networked and Distributed Computing. 2. 1-6. 10.1109/SNPD.2014.6888709.
\bibitem{c13}Søndergaard, Harald \& Sestoft, Peter. (1989). Referential Transparency, Definiteness and Unfoldability.. Acta Inf.. 27. 505-517. 10.1007/BF00277387. 
\bibitem{c14}Simon Marlow, Patrick Maier, Hans-Wolfgang Loidl, Mustafa K. Aswad, and Phil Trinder. 2010. Seq no more: better strategies for parallel Haskell. In Proceedings of the third ACM Haskell symposium on Haskell (Haskell '10). ACM, New York, NY, USA, 91-102. DOI:
\bibitem{c15}Wadler P. (1993) Monads for functional programming. In: Broy M. (eds) Program Design Calculi. NATO ASI Series (Series F: Computer and Systems Sciences), vol 118. Springer, Berlin, Heidelberg
\bibitem{c16}Aaron Weiss \& Daniel Patterson \& Nicholas D. Matsakis \& Amal Ahmed. 2019. Oxide: The Essence of Rust. CoRR 2019
\bibitem{c17} Chakravarty, Manuel \& Keller, Gabriele \& Lee, Sean \& McDonell, Trevor \& Grover, Vinod. (2011). Accelerating Haskell array codes with multicore GPUs. DAMP'11 - Proceedings of the 6th ACM Workshop on Declarative Aspects of Multicore Programming. 3-14. 10.1145/1926354.1926358. 







\end{thebibliography}




\end{document}
